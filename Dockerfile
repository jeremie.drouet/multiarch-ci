ARG IMAGE_ARCH=amd64

FROM ${IMAGE_ARCH}/node

ENV PORT 80

COPY . /code
WORKDIR /code

RUN /usr/local/bin/npm install

ENTRYPOINT ["/usr/local/bin/npm", "start"]
